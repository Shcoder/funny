<?php

class m121224_050912_create_data_menu extends CDbMigration
{
	public function up()
	{
		$this->createTable('data_menu', array(
		    'id'         => 'pk',
		    'title'      => 'string NOT NULL',
		    'link'       => 'string NOT NULL',
		    'created_at' => 'integer(11) NOT NULL',
		    'status'     => 'boolean NOT NULL DEFAULT 1',
		    'weight'     => 'integer NOT NULL DEFAULT 0',
		));
	}

	public function down()
	{
		$this->dropTable('data_menu');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}