<?php

class m121223_121020_create_admin_menu extends CDbMigration
{
	public function up()
	{
		$this->createTable('data_admin_menu', array(
            'id'         => 'pk',
            'title'      => 'string NOT NULL',
            'link'       => 'string NOT NULL',
            'created_at' => 'integer(11) NOT NULL',
            'status'     => 'boolean NOT NULL DEFAULT 1',
            'weight'     => 'integer NOT NULL DEFAULT 0',
        ));
	}

	public function down()
	{
		$this->dropTable('data_admin_menu');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}