<?php

class m121224_053432_create_data_page extends CDbMigration
{
	public function up()
	{
		$this->createTable('data_page', array(
		    'id'         => 'pk',
		    'title'      => 'string NOT NULL',
		    'text'       => 'text',
		    'created_at' => 'integer(11) NOT NULL',
		    'status'     => 'boolean NOT NULL DEFAULT 1',
		    'weight'     => 'integer NOT NULL DEFAULT 0',
		));
	}

	public function down()
	{
		$this->dropTable('data_page');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}