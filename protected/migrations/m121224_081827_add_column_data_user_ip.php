<?php

class m121224_081827_add_column_data_user_ip extends CDbMigration
{
	public function up()
	{
		$this->addColumn('data_user', 'ip', 'string NULL');
	}

	public function down()
	{
		$this->dropColumn('data_user', 'ip');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}