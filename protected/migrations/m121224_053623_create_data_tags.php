<?php

class m121224_053623_create_data_tags extends CDbMigration
{
	public function up()
	{
		$this->createTable('data_tags', array(
		    'id'         => 'pk',
		    'tags'       => 'string NOT NULL',
		    'created_at' => 'integer(11) NOT NULL',
		    'status'     => 'boolean NOT NULL DEFAULT 1',
		    'weight'     => 'integer NOT NULL DEFAULT 0',
		));
	}

	public function down()
	{
		$this->dropTable('data_tags');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}