<?php

class m121224_053448_create_data_core extends CDbMigration
{
	public function up()
	{
		$this->createTable('data_core', array(
		    'id'         => 'pk',
		    'created_at' => 'integer(11) NOT NULL',
		    'status'     => 'boolean NOT NULL DEFAULT 1',
		    'weight'     => 'integer NOT NULL DEFAULT 0',
		));
	}

	public function down()
	{
		$this->dropTable('data_core');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}