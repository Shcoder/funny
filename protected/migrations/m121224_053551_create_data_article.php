<?php

class m121224_053551_create_data_article extends CDbMigration
{
	public function up()
	{
		$this->createTable('data_article', array(
		    'id'         => 'pk',
		    'title'      => 'string NOT NULL',
		    'desc'       => 'text',
		    'text'       => 'text',
		    'created_at' => 'integer(11) NOT NULL',
		    'status'     => 'boolean NOT NULL DEFAULT 1',
		    'weight'     => 'integer NOT NULL DEFAULT 0',
		));
	}

	public function down()
	{
		$this->dropTable('data_article');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}