<?php

class m121224_053504_create_data_settings extends CDbMigration
{
	public function up()
	{
		$this->createTable('data_settings', array(
		    'id' => 'pk',
		    'title' => 'string NOT NULL',
		    'name' => 'string NOT NULL',
		    'group' => 'string NOT NULL',
		    'value' => 'string NOT NULL',
		    'created_at' => 'integer(11) NOT NULL',
		    'status' => 'boolean NOT NULL DEFAULT 1',
		    'weight' => 'integer NOT NULL DEFAULT 0',
		));
	}

	public function down()
	{
		$this->dropTable('data_settings');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}