<?php

class m121224_080652_create_data_user extends CDbMigration
{
	public function up()
	{
		$this->createTable('data_user', array(
		    'id' => 'pk',
		    'name' => 'string',
		    'login' => 'string NOT NULL unique',
		    'email' => 'string NOT NULL unique',
		    'role' => 'string',
		    'password' => 'string',
		    'created_at' => 'integer(11) NOT NULL',
		    'lastbeen_at' => 'integer(11) NOT NULL',
		    'status' => 'boolean NOT NULL DEFAULT 1',
		));
	}

	public function down()
	{
		$this->dropTable('data_user');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}