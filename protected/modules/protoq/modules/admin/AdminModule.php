<?php

class AdminModule extends CWebModule
{
    public function init()
	{
        // this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
            'application.modules.protoq.models.*',
            'application.modules.protoq.components.*',
            'application.modules.protoq.controllers.*',
            'application.modules.protoq.modules.admin.models.*',
            'application.modules.protoq.modules.admin.components.*',
            'application.modules.protoq.modules.admin.controllers.*',            
		));

          // $this->registerAssets();
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
