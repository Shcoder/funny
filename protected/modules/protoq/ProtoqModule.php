<?php

class ProtoqModule extends CWebModule
{
    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
                $this->getId().'.models.*',
                $this->getId().'.components.*',
        ));

        $this->modules = array(
        	'admin'=>array(
				'class'=>'application.modules.protoq.modules.admin.AdminModule',
			),
        );

        $mpath = Yii::getPathOfAlias('application.modules.protoq.modules');
        YiiBase::setPathOfAlias('modules', $mpath);


    }

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
